from django.db import models


# Create your models here.


class TB_Cliente(models.Model):
    cpf = models.CharField(max_length=14)
    nome = models.CharField(max_length=100)
    email = models.EmailField()
    endereco = models.CharField(max_length=100)
    fone = models.CharField(max_length=11)
    dt_nascimento = models.DateField()
    sexo = models.CharField(max_length=4, choices=[
        ("MASC", "Masculino"),
        ("FEM", "Feminino"),
        ("OUT", "Outro")
    ], default="Masculino")


class TB_Livro(models.Model):
    isbn = models.CharField(max_length=17)
    titulo = models.CharField(max_length=100)
    autor = models.CharField(max_length=100)
    editora = models.CharField(max_length=100)
    ano_de_publicacao = models.DateField()
    preco = models.DecimalField(max_digits=6, decimal_places=2)
    quantidade = models.PositiveIntegerField()
