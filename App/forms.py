from django.forms import ModelForm, DateField, SelectDateWidget
from django.utils.translation import ugettext_lazy as _
from .models import *


class FormCliente(ModelForm):
    class Meta:
        model = TB_Cliente
        fields = "__all__"
        labels = {
            'cpf': 'CPF:',
            'email': 'E-mail:',
            'endereco': 'Endereço:',
            'Fone': 'Telefone:'
        }

    dt_nascimento = DateField(
        widget=SelectDateWidget(attrs={'class': 'datepicker'}),
        input_formats=('%d/%m/%Y',),
        label="Data de Nascimento:"
    )


class FormLivro(ModelForm):
    class Meta:
        model = TB_Livro
        fields = "__all__"
        labels = {
            'isbn': _('ISBN:'),
            'preco': _('Preço:')
        }

    ano_de_publicacao = DateField(
        widget=SelectDateWidget(attrs={'class': 'datepicker'}),
        input_formats=('%d/%m/%Y',),
        label="Ano de Publicação:"
    )
