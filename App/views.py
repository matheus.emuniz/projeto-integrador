from django.shortcuts import render, redirect
from .forms import *


# Create your views here.


def cadastrar_livro(request):
    if request.method == "GET":
        form = FormLivro()
        return render(request, 'cadastrar_livro.html', context={'form': form})
    else:
        form = FormLivro(request.POST)
        if form.is_valid():
            form.save()
            return redirect('lista')
        else:
            return render(request, 'cadastrar_livro.html', context={'form': form})


def cadastrar_cliente(request):
    if request.method == "GET":
        form = FormCliente()
        return render(request, 'cadastrar_cliente.html', context={'form': form})
    else:
        form = FormCliente(request.POST)
        if form.is_valid():
            form.save()
            return redirect('lista')
        else:
            return render(request, 'cadastrar_cliente.html', context={'form': form})


def update_livro(request, pk):
    livro = TB_Livro.objects.get(pk=pk)
    form = FormLivro(request.POST or None, instance=livro)
    if form.is_valid():
        form.save()
        return redirect('lista')
    return render(request, 'update_livro.html', context={'form': form})


def update_cliente(request, pk):
    cliente = TB_Cliente.objects.get(pk=pk)
    form = FormCliente(request.POST or None, instance=cliente)
    if form.is_valid():
        form.save()
        return redirect('lista')
    return render(request, 'update_cliente.html', context={'form': form})


def delete_livro(request, pk):
    livro = TB_Livro.objects.get(pk=pk)
    livro.delete()
    return redirect('lista')

def delete_cliente(request, pk):
    cliente = TB_Cliente.objects.get(pk=pk)
    cliente.delete()
    return redirect('lista')


def home(request):
    return render(request, 'home.html')


def lista(request):
    clientes = TB_Cliente.objects.all()
    livros = TB_Livro.objects.all()
    return render(request, 'lista.html', context={'clientes': clientes, 'livros': livros})
